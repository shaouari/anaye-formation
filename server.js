var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var app = express();

// view engine 
app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs');
app.engine('html',require('ejs').renderFile);

//static folder
app.use(express.static(path.join(__dirname, 'public')));


//Body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// api
require('./routes/equipe')(app);

app.all('/api/*',function(req,res){
      res.send(404);
  });


app.get('*', function(req,res){
    res.render('index.html');
});



app.listen(3000, function () {
  console.log('app listening on port 3000!')
})