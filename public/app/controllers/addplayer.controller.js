  
var app = angular.module('EquipeFoot');

'use strict';

  app.controller('AddPlayerController' , function($scope,$http,EquipeFactory){

        EquipeFactory.getEquipe().then(
               function(data){
                   $scope.equipe = data;
               }
           );

       $scope.addPlayer = function(){

                $scope.error = null;
               // ng-if , ng-hide , ng-show
                var exist = _.findWhere($scope.equipe, { prenom: $scope.newPlayer.prenom});

                //undefined , null , false
                if(exist){
                  $scope.error = 'Le joueur ' + exist.prenom + ' existe déja' ;
                  return;
                }

                $scope.equipe.push($scope.newPlayer);

                // reset form
                $scope.newPlayer = {};
                $scope.canAdd = false;

        }
        
 });