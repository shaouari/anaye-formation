 
  var app = angular.module('EquipeFoot');

  app.factory('EquipeFactory',function($q,$http){

    var _equipe = null;

    var _getEquipe  = function(){

        var deferred = $q.defer();
        if(_equipe){
             deferred.resolve(_equipe);
            
        }else{
            $http.get('api/equipe').then(function(resp){
                _equipe = resp.data;
                deferred.resolve(_equipe);
            }).catch(function(){

                deferred.reject();
            });
        }
        return deferred.promise;
    }

    var _muliplier = function(a, b){
        return a*b;
    }

    return {
        getEquipe : _getEquipe,
        multiplier : _muliplier
     }

  })
