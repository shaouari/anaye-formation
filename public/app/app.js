angular.module('EquipeFoot',['ui.router'])
.config(function($stateProvider){

    var homePage = {
        name : 'home',
        url : '/',
        template : '<div>Home Page</div>'
    };
    var  equipePage = {
        name : 'equipe',
        url : '/equipe',
        templateUrl : 'app/templates/equipe.html',
        controller : 'EquipeController'
    };
     var addPlayerPage = {
        name : 'addplayer',
        url : '/addplayer',
        templateUrl : 'app/templates/add.html',
        controller : 'AddPlayerController'
    };

    var newPage = {
        name : 'test',
        url : '/test',
        templateUrl : 'app/templates/test.html',
        controller : 'TestController'
    };

    $stateProvider.state(homePage);
    $stateProvider.state(equipePage);
    $stateProvider.state(addPlayerPage);
    $stateProvider.state(newPage);
});