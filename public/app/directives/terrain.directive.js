 var app = angular.module("EquipeFoot")

 app.directive('terrainVert',function(){
     return {
         restrict : 'EA', // Element - Attribute - les deux
         transclude: true,
         scope : {
             largeur : '@', // = : par valeur , @ la chaine , &  
             hauteur : '@',
         },
         templateUrl : 'app/directives/terrain.html',
         controller : function($scope){
            $scope.tracer = true;
            $scope.color= 'green';
            console.log('Terrain : Controller');
            $scope.removeTrace = function(){
                $scope.tracer = false;
            } 

             $scope.$on('CHANGE::COLOR',function(event,color){
                 $scope.color = color;
             });
         },

     }
 })