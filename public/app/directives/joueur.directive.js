 var app = angular.module("EquipeFoot")

 app.directive('joueur',function(){
     return {
         restrict : 'E', // Element - Attribute - les deux
         scope : {
             height : '@', // = : par valeur , @ la chaine , &  
             width : '@',
             color:'@',
             nom:'@'
         },
         templateUrl : 'app/directives/joueur.html',
         controller : function($scope){
         
             $scope.remove = function(){
                $scope.$emit('REMOVE::PLAYER',$scope.nom);
             }

             $scope.$on('CHANGE::COLOR',function(event,color){
                 $scope.color = color;
             });
         },

     }
 })