var app = angular.module('EquipeFoot');

app.directive('complex',function(){
    return {
        restrict : 'EA',
        transclude : true,
        scope : {
            name : '@'
        },
        templateUrl : 'app/directives/complex.html',
        controller : function($scope){

         },
    }

});