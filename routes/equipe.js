var express = require('express');

var router =  express.Router();

var equipe = [{nom : 'Toufiki' , prenom : 'Mounir' , post : 'Defense' },{nom : 'Bergia' , prenom : 'Anouar' , post : 'Defense' },{nom : 'Ifzarne' , prenom : 'Hanae' , post : 'Blessé' },{nom : 'Mouadili' , prenom : 'Merouane' , post : 'Milieu' }]

module.exports = function(app){

    app.get('/api/equipe',function(req,res){
  
        res.send(equipe);
    });

}